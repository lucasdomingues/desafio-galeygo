#Crie uma função que receba duas listas e retorne uma terceira com os elementos intercalados: [a,b,c], [1,2,3] → [a,1,b,2,c,3]

def main():
    print(concatena_listas([1,2,3], ["a","b", "c", "d"]))


def concatena_listas(listaA, listaB):

    listaC = []

    if (len(listaA) >= len(listaB)):
        n = len(listaA)
        for i in range(n):
            listaC.append(listaA[i])
            if (i < len(listaB)):
                listaC.append(listaB[i])
        return listaC
    elif (len(listaA) < len(listaB)):
        n = len(listaB)
        for i in range(n) :
            listaC.append(listaB[i])
            if (i < len(listaA)):
                listaC.append(listaA[i])
        return listaC

if __name__ == '__main__':
    main()