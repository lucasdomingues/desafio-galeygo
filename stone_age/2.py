#Crie uma função que receba um inteiro n e calcula a soma de todos os múltiplos de 3 de 1 até n



def retorna_soma_dos_multiplis_de_3(n):
    total = 0;
    for number in range(1 , n):
        if (number % 3 == 0):
            total = total + number
    print(total)


def main():
    retorna_soma_dos_multiplis_de_3(10)


if __name__ == '__main__':
    main()