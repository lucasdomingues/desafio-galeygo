#Crie uma função que retorna o maior valor de uma lista

import sys

def main():

    print("Olá mundo")
    myList = [1, 2, 3, 4, 7, 0, 209450, 4, 9]
    max = -sys.maxsize -1
    for number in myList:
        if (number > max):
            max = number
    print (max)

if __name__ == '__main__':
    main()