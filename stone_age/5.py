#O maior palíndromo que é produto de dois números de 2 dígitos é 9009 = 91 × 99. Ache o maior palíndromo feito do produto de dois números de 3 dígitos

from stone_age.exercicio4 import *

def eh_palindromo2 (texto) :
    texto.replace(" ", "")
    invertido = texto[::-1].replace(" ", "")
    return (texto.upper() == invertido.upper())

def main():

    for i in range(999, 100, -1) :
        for j in range (999, 100, -1) :
            valor = i * j
            if(eh_palindromo(str(valor))):
                print (i ,"x", j)
                print(valor)
                return valor;
    return -1;


if __name__ == '__main__':
    main()