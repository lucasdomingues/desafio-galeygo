# Galeygo #

## Desafio 1 ##

Você voltou para a época das cavernas! Você só dispõe de python puro e não pode usar nenhuma das funções roubadas do python (max, min, sort, reverse, etc...)

## Como começar ? ##
1. Faça um fork desse projeto
2. Escreva o código de cada problema no arquivo correspondente da pasta stone_age. Ex.: stone_age/1.py

## Problemas ##
1. Crie uma função que retorna o maior valor de uma lista
2. Crie uma função que receba um inteiro n e calcula a soma de todos os múltiplos de 3 de 1 até n
3. Crie uma função que receba duas listas e retorne uma terceira com os elementos intercalados: [a,b,c], [1,2,3] → [a,1,b,2,c,3]
4. Crie uma função que diga se uma string é um ([palíndromo](https://pt.wikipedia.org/wiki/Palíndromo))
5. O maior palíndromo que é produto de dois números de 2 dígitos é 9009 = 91 × 99. Ache o maior palíndromo feito do produto de dois números de 3 dígitos

##![newchallenger.png](https://bitbucket.org/repo/gzonLB/images/2446145837-newchallenger.png)##

## Desafio 2 ##

Estamos fazendo um novo admin, utilizando django, pra cuidar dos processos internos da nossa equipe de operações.
Ainda estamos no início então há muita coisa a ser feita.
Aqui você pode usar qualquer coisa que resolva o problema, bibliotecas, apps etc.

## Como começar ? ##
1. Faça um fork desse projeto caso não tenha feito
2. Crie um virtualenv e execute: `pip install -r requirements.txt`
3. Dê runserver e vá para localhost:8000/admin que é onde você verá sua mudanças
4. Mostre do que é capaz ! =D

## Os seguintes aspectos do seu projeto serão avaliados: ##
* Agilidade.
* Legibilidade.
* Escopo.
* Organização do código.
* Existência e quantidade de bugs e gambiarras.
* Testes, testes, teste...

## O que o desafio propõe ##



1. Mapear os models e admins que julgar necessário.
2. Nossa aplicação tem que estar em pt-br.
3. Dentro da tabela Products, existe o campo UnitPrice, então crie uma action no admin onde o usuário possa modificar esse valor para n produtos.
4. Dentro da tabela Customers, existe o campo Region, adicione no admin do Customers um filtro pela região.
5. Dentro da tabela Employees, existe o campo HireDate, crie um filtro por esse campo.
6. Dentro da tabela Customers, existe o campo Country, adicione um filtro pela país, mas que só mostre países com até 3 caracteres.
7. Adicione os admins no menu da aplicação.
8. Existe um bug no modelo Region já mapeado! É possível preencher o id ao editar ou adicionar um novo. Se existir algum outro registro com aquele id um erro ocorrerá. Resolva esse bug.
9. De alguma forma mostre para cada Supplicar a quantidade de Products com UnitPrice acima de 50, dica? View e template! (shiuuuuuu, ninguém disse nada...)
10. Instale pelo menos uma biblioteca auxiliar a parte. Ex.: django-debug-toolbar para analisar o desempenho do admin.


Obs: Já existe um banco, `Northwind.sl3`, nele já existe as tabelas campos e registros para o desenvolvimento do projeto !



![Northwind.png](https://bitbucket.org/repo/gzonLB/images/296778867-Northwind.png)