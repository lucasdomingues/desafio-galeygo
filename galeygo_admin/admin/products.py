

from galeygo_admin.form import AlterarUnitPrice
from django.contrib import admin

from django.contrib import messages
from galeygo_admin.models import Product


def action_product(self, request, queryset):
   newPrice = float(request.POST['unit_price'])
   queryset.update(unitprice=newPrice)
   messages.success(request, '{0} movies were updated'.format(queryset.count()))


action_product.short_description = 'Editar valor'


class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ('productid', 'productname', 'unitprice')
    #list_editable = ('productid','unitprice')
    action_form = AlterarUnitPrice
    actions = [action_product]
