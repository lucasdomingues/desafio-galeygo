# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.models import Customer


class CountryListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = ('Country')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'country'

    def lookups(self, request, model_admin):
        list_tuple = []
        for c in Customer.objects.all():
            if len(c.country) <= 3:
                list_tuple.append((c.country, c.country))
        return tuple(set(list_tuple))


    def queryset(self, request, queryset):
        list = []
        for c in Customer.objects.all():
            if len(c.country) <= 3:
                list.append(c.country)
        countries = set(list)
        if self.value() in countries:
            return queryset.filter(country=self.value())
        else:
            return queryset


class CustomerAdmin(admin.ModelAdmin):
    model = Customer
    list_display = ('customerid', 'companyname', 'country', 'region')
    list_filter = [CountryListFilter, 'region']
