# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.models import Region

class RegionAdmin(admin.ModelAdmin):
    model = Region
    list_display = ('id', 'region_description')
