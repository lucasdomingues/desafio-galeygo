# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.models import Employee

class EmployeeAdmin(admin.ModelAdmin):
    model = Employee
    list_display = ('employeeid', 'firstname')
