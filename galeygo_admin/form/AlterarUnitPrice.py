from django.contrib.admin.helpers import ActionForm
from django import forms


class AlterarUnitPrice(ActionForm):
    unit_price = forms.FloatField()